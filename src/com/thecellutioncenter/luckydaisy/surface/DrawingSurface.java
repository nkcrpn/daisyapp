package com.thecellutioncenter.luckydaisy.surface;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.thecellutioncenter.luckydaisy.items.FlowerHead;
import com.thecellutioncenter.luckydaisy.items.FlowerPetal;
import com.thecellutioncenter.luckydaisy.tools.MyConstants;

public class DrawingSurface extends SurfaceView implements SurfaceHolder.Callback {

	private float CENTER_X;
	private float CENTER_Y;
	private int RADIUS;

	private ViewThread viewThread;
	private Matrix matrix;
	private Rect fullScreenRect;
	private Paint paint;

	private List<FlowerPetal> petals = new ArrayList<FlowerPetal>();
	private FlowerHead flowerHead;

	private Bitmap grassBackgroundBitmap;
	private boolean isLiked;
	private boolean isMale = false;

	public DrawingSurface(Context context) {
		super(context);
		viewThread = new ViewThread(getHolder(), this);

		matrix = new Matrix();
		paint = new Paint();

		/* TODO Why is this necessary to let TouchEvent work? */
		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		initBitmaps(context);

		getHolder().addCallback(this);
		setFocusable(true);
	}

	private void initBitmaps(Context context) {
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = (TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics()));
		}

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		CENTER_X = metrics.widthPixels / 2;
		CENTER_Y = (metrics.heightPixels / 2) - actionBarHeight;

		fullScreenRect = new Rect(0, 0, metrics.widthPixels, metrics.heightPixels);
		grassBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blurry_grass);

		/*
		 * TODO Use bigger image of Daisy and use better petal / center This
		 * would be able to scale better, then
		 */
		Bitmap flowerHeadBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flower_head);

		RADIUS = flowerHeadBitmap.getWidth();
		float headScale = RADIUS * .05f;
		float headLeft = CENTER_X - (RADIUS / 2);
		float headTop = CENTER_Y - (RADIUS / 2);

		flowerHead = new FlowerHead(flowerHeadBitmap);
		flowerHead.set(headLeft - headScale, headTop - headScale, headLeft + RADIUS + headScale, headTop + RADIUS + headScale);

		/* TODO: This belongs in the model.
		 * Return a list of formatted petals.
		 */
		Bitmap petalBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.petal);
		for (int angle = 0; angle < MyConstants.MAX_ANGLE; angle += MyConstants.DELTA_ANGLE) {
			matrix.reset();
			matrix.postRotate(angle);

			Bitmap newPetalBitmap = Bitmap.createBitmap(petalBitmap, 0, 0, petalBitmap.getWidth(), petalBitmap.getHeight(), matrix, false);

			FlowerPetal petal = new FlowerPetal(newPetalBitmap);

			int dx = (int) (RADIUS * Math.cos(Math.toRadians(angle)));
			int dy = (int) (RADIUS * Math.sin(Math.toRadians(angle)));

			float left = CENTER_X + dx - (petal.getWidth() / 2);
			float top = CENTER_Y + dy - (petal.getHeight() / 2);
			petal.set(left, top, left + petal.getWidth(), top + petal.getHeight());

			petals.add(petal);
		}
		
		initializeLiked();
	}
	
	private void initializeLiked() {
		isLiked = (petals.size() % 2 == 0) ? true : false;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		/* TODO null canvas is passed in on close */
		if (canvas == null) {
			return;
		}

		canvas.drawBitmap(grassBackgroundBitmap, null, fullScreenRect, null);

		paint.setColor(Color.BLACK);
		/* TODO: Text size should scale with screen size.
		 * This works fine for small phones, but
		 * is incredibly small for the really HD phones.
		 */
		paint.setTextSize(30);
		canvas.drawText(getCurrText(), CENTER_X - (CENTER_X * .85f), CENTER_Y - (CENTER_Y * .85f), paint);

		for (FlowerPetal petal : petals) {
			petal.draw(canvas);
		}

		flowerHead.draw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			/* TODO: No point in looping through petals
			 * that have already been clicked.
			 */
			for (FlowerPetal petal : petals) {
				if (petal.isVisible() && petal.contains(event.getX(), event.getY())) {
					petal.click();
					toggleLike();
				}
			}
			return true;
		} else {
			return super.onTouchEvent(event);
		}
	}

	private String getCurrText() {
		/* TODO: View should not be holding state this much.
		 * I expect this to change once refactored to MVP.
		 * Also, refactor the constants somehow.
		 */
		if(isLiked && isMale) {
			return MyConstants.MALE_POSITIVE;
		} else if (!isLiked && isMale) {
			return MyConstants.MALE_NEGATIVE;
		} else if (isLiked && !isMale) {
			return MyConstants.FEMALE_POSITIVE;
		} else {
			return MyConstants.FEMALE_NEGATIVE;
		}
	}


	public void reset() {
		for(FlowerPetal petal : petals){
			petal.setVisible(true);
		}
		initializeLiked();
	}

	public void changeGender() {
		isMale = !isMale;
	}
	
	public void toggleLike() {
		isLiked = !isLiked;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		viewThread.setRunning(true);
		viewThread.start();
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		viewThread.setRunning(false);
		try {
			viewThread.join();
		} catch (InterruptedException e) {
			
		}
	}
}