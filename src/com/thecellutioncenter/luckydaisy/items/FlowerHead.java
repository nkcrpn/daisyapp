package com.thecellutioncenter.luckydaisy.items;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class FlowerHead extends FlowerItem {

	public FlowerHead(Bitmap bitmap) {
		super(bitmap);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(getBitmap(), null, this, null);
	}

	@Override
	public void click() {
		
	}
}
