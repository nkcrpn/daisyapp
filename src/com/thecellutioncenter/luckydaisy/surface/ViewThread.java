package com.thecellutioncenter.luckydaisy.surface;


import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class ViewThread extends Thread {
	private SurfaceHolder _surfaceHolder;
    private DrawingSurface _panel;
    private boolean _run = false;
 
    public ViewThread(SurfaceHolder surfaceHolder, DrawingSurface homeView) {
        _surfaceHolder = surfaceHolder;
        _panel = homeView;
    }
 
    public void setRunning(boolean run) {
        _run = run;
    }
 
    @SuppressLint("WrongCall")
	@Override
    public void run() {
    	
        Canvas c;
        while (_run) {
            c = null;
            try {
                c = _surfaceHolder.lockCanvas(null);
                synchronized (_surfaceHolder) {
                    _panel.onDraw(c);
                    
                    try {
            			sleep(30);
            		} catch (InterruptedException e) {
            			e.printStackTrace();
            		}
                }
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
}
