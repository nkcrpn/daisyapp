package com.thecellutioncenter.luckydaisy;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class HomeActivity extends Activity {

	HomeView view;
	HomeModel model;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* TODO: Refactor to use MVP */
		view = new HomeView(this);
		model = new HomeModel();

		HomePresenter.create(view, model);

		setContentView(view.getView());
	}

	/* TODO: Implement option to choose number of petals */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.reset:
			view.reset();
			return true;
		case R.id.gender:
			view.changeGender();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}
}
