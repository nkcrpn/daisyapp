package com.thecellutioncenter.luckydaisy;

import com.thecellutioncenter.luckydaisy.surface.DrawingSurface;

import android.content.Context;


public class HomeView {

	DrawingSurface view;
	
	public HomeView(Context context) {
		view = new DrawingSurface(context);
	}
	
	public DrawingSurface getView() {
		return view;
	}

	public void reset() {
		view.reset();
	}

	public void changeGender() {
		view.changeGender();
	}
}
