package com.thecellutioncenter.luckydaisy.items;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

public abstract class FlowerItem extends RectF {
	
	private Bitmap myImage;
	private boolean isVisible;
	
	public FlowerItem(Bitmap bitmap) {
		myImage = bitmap;
		isVisible = true;
	}
	
	public abstract void draw(Canvas c);
	
	public Bitmap getBitmap() {
		return myImage;
	}
	
	public float getWidth() {
		return myImage.getWidth();
	}
	
	public float getHeight() {
		return myImage.getHeight();
	}

	public abstract void click();
	
	public boolean isVisible() {
		return isVisible;
	}
	
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
