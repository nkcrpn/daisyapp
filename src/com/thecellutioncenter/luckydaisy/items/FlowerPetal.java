package com.thecellutioncenter.luckydaisy.items;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class FlowerPetal extends FlowerItem {


	public FlowerPetal(Bitmap bitmap) {
		super(bitmap);
	}

	@Override
	public void draw(Canvas canvas) {
		if (isVisible()) {
			canvas.drawBitmap(getBitmap(), left, top, null);
		}
	}

	@Override
	public void click() {
		setVisible(false);
	}

	@Override
	public boolean contains(float x, float y) {
		boolean isContained = super.contains(x, y);
		if (isContained) {
			int bmpXCoord = (int) (x - left);
			int bmpYCoord = (int) (y - top);
			
			/* True if the color at the coordinate is not completely alpha
			 * ie: It's a click on the petal and not surrounding empty space
			 * since a petal is technically a rectangle.
			 * Perhaps this isn't the best implementation, but it's easier
			 * and more reliable than trying to figure out some mathematical
			 * equation for where the petal is based on the width / height of image
			 * and angle of petal.
			 */
			isContained = getBitmap().getPixel(bmpXCoord, bmpYCoord) != 0;
		}
		
		return isContained;
	}

}
