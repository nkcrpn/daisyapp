package com.thecellutioncenter.luckydaisy.tools;

public class MyConstants {
	
	public static final String FEMALE_POSITIVE = "He likes me!";
	public static final String FEMALE_NEGATIVE = "He likes me not.";
	public static final String MALE_POSITIVE = "She likes me!";
	public static final String MALE_NEGATIVE = "She likes me not.";
	
	public static final int MAX_ANGLE = 360;
	public static final int DELTA_ANGLE = 15;
}
